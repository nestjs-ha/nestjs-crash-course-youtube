import { Injectable, Inject } from '@nestjs/common';
import { Item } from './interfaces/item.interface';
import { Model } from 'mongoose';
import { CreateItemDto } from './dto/create-item.dto';

@Injectable()
export class ItemsService {
    // private readonly items: Item[] = [
    //     {
    //      name: 'Item One',
    //      qty: 100,
    //      description: 'This is item one',
    //     },
    //     {
    //         name: 'Item Two',
    //         qty: 200,
    //     },
    // ];
    constructor(
        @Inject('ITEM_MODEL') private readonly itemModel: Model<Item>) {

    }

    async create( createItemDto: CreateItemDto): Promise<Item> {
        const createdItem = new this.itemModel(createItemDto);
        await createdItem.save();
        return createdItem;
    }

    async findAll(): Promise<Item[]> {
            return await this.itemModel.find();
        }

    async findOne(id: string): Promise<Item> {
        return await this.itemModel.findOne({ _id: id});
    }

    async delete(id: string): Promise<Item> {
        return await this.itemModel.findByIdAndRemove(id);
    }

    async update( id: string, item: CreateItemDto): Promise<Item> {
        return await this.itemModel.findByIdAndUpdate(id, item, { new: true });
    }
}
